clear all
clc

%%
%visualization of horizontal slices

[exam,exam_seg] = load_case( 10 , 'nor1');
Image = mat2gray(exam.Case.data);

Image = permute(Image, [1 2 4 3]);
Image_seg = permute(exam_seg.Case_seg.data, [1 2 4 3]);

figure('units','normalized','outerposition',[0 0 1 1])
subplot(121); montage(Image);
title('Horizontal');
subplot(122); montage(Image_seg);
title('Horizontal mask');

%creating sagittal slices and visualization

% M1 = Image(:,size(Image,1)/2,:,:);
% M2 = reshape(M1,[size(Image,1) size(Image,4)]);
% figure, imshow(M2);
% title('Sagittal - Raw Data');
% 
% T0 = maketform('affine',[0 -2.5; 1 0; 0 0]);
% R2 = makeresampler({'cubic','nearest'},'fill');
% M3 = imtransform(M2,T0,R2);  
% figure, imshow(M3);
% title('Sagittal - IMTRANSFORM')

%% based on: Compute 3-D superpixels of input volumetric intensity image

%Load 3-D MRI data, remove any singleton dimensions, and convert the data into a grayscale intensity image.
D = squeeze(Image);
A = mat2gray(D);

%Calculate the 3-D superpixels. Form an output image where each pixel is set to the mean color of its corresponding superpixel region.
[L,N] = superpixels3(A, 70);

% Show all xy-planes progressively with superpixel boundaries.
imSize = size(A);

%Create a stack of RGB images to display the boundaries in color.
imPlusBoundaries = zeros(imSize(1),imSize(2),3,imSize(3),'uint8');
for plane = 1:imSize(3)
  BW = boundarymask(L(:, :, plane));
  % Create an RGB representation of this plane with boundary shown
  % in cyan.
  imPlusBoundaries(:, :, :, plane) = imoverlay(A(:, :, plane), BW, 'cyan');
end

implay(imPlusBoundaries,5)

keyboard

%Load 3-D MRI data, remove any singleton dimensions, and convert the data into a grayscale intensity image.
D = squeeze(Image_seg);
A = mat2gray(D);

%Calculate the 3-D superpixels. Form an output image where each pixel is set to the mean color of its corresponding superpixel region.
[L,N] = superpixels3(A,15);

% Show all xy-planes progressively with superpixel boundaries.
imSize = size(A);

%Create a stack of RGB images to display the boundaries in color.
imPlusBoundaries = zeros(imSize(1),imSize(2),3,imSize(3),'uint8');
for plane = 1:imSize(3)
  BW = boundarymask(L(:, :, plane));
  % Create an RGB representation of this plane with boundary shown
  % in cyan.
  imPlusBoundaries(:, :, :, plane) = imoverlay(A(:, :, plane), BW, 'cyan');
end

implay(imPlusBoundaries,5)

%%

for i = 0:49
    [exam,exam_seg] = load_case( i , 'norm');
    I = exam.Case.data(:,:,ceil(size(exam.Case.data,3)/2));
    I_seg = exam_seg.Case_seg.data(:,:,ceil(size(exam.Case.data,3)/2));
    figure, imshow(I);
    h = imellipse;
    position = wait(h);
    init_mask = createMask(h);
    close
    mask = activecontour(I, init_mask,'edge','ContractionBias',0.4);
    
    %expand (dilate) ROI to 120%
    stats = regionprops(mask,'MajorAxisLength','MinorAxisLength');
    se_size = round((stats.MajorAxisLength + stats.MinorAxisLength)/10);
    se = strel('disk',se_size);
    dilated_mask = imdilate(mask,se);
    
%     masks = zeros(size(exam.Case.data,1),size(exam.Case.data,2),size(exam.Case.data,3));
%     for m=1:size(exam.Case.data,3)
%         new_mask = activecontour(exam.Case.data(:,:,m), init_mask);
% %         masks(:,:,m) = new_mask;
%         figure('units','normalized','outerposition',[0 0 1 1])
%         subplot(131), imshow(exam.Case.data(:,:,m));
%         subplot(132), imshow(new_mask);
%         subplot(133), imshow(exam_seg.Case_seg.data(:,:,m));
%     end
%     masks = logical(masks);
% 
%     permmasks = permute(masks, [1 2 4 3]);
%     figure('units','normalized','outerposition',[0 0 1 1])
%     montage(permmasks);
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(221), imshow(mask)
    title("mask");
    subplot(222), imshow(dilated_mask)
    title("dilated mask");
    mask_db = double(dilated_mask);
    subplot(223), imshow(mask_db-I_seg,[-1 1]);
    title(strcat('dilated mask - seg, -1: ',num2str(sum(sum((mask_db-I_seg)==-1)))));
    subplot(224), imshow(I_seg)
    title("ref seg");
    suptitle(strcat('Case ',num2str(i)));
end

%%
%3D superpixels

%Load 3-D MRI data, remove any singleton dimensions, and convert the data into a grayscale intensity image.
D = squeeze(Image);
A = mat2gray(D);

[L,NumLabels] = superpixels3(A,50);

% Show all xy-planes progressively with superpixel boundaries.
imSize = size(A);

%Create a stack of RGB images to display the boundaries in color.
imPlusBoundaries = zeros(imSize(1),imSize(2),3,imSize(3),'uint8');
for plane = 1:imSize(3)
  BW = boundarymask(L(:, :, plane));
  % Create an RGB representation of this plane with boundary shown
  % in cyan.
  imPlusBoundaries(:, :, :, plane) = imoverlay(A(:, :, plane), BW, 'cyan');
end

implay(imPlusBoundaries,5)

%%
figure
BW = boundarymask(L);
imshow(imoverlay(I,BW,'cyan'),'InitialMagnification',67)
