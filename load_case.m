function [ exam, exam_seg ] = load_case(case_num , type)
%Function load_case allows to get train or test data from .mat files
%args: case_num - int, number of desired case
%      type - String, type of data: 
%                   'raws' - raw pictures
%                   'nor0' - normalized with z-score and values (0 - 1)
%                   'nor1' - normalized with N4ITK
%@returns: exam - struct, cointains image data and metadata
%          exam_seg - struct, contains segmentation mask
    if type == 'raws'
        if case_num < 0 | case_num>49
            disp('Incorrect train case number!');    
        elseif case_num < 10
            dir = strcat('Data\TrainingMatData\Case0',num2str(case_num),'.mat');
            exam = load(dir);
            dir_seg = strcat('Data\TrainingMatData\Case0',num2str(case_num),'_seg.mat');
            exam_seg = load(dir_seg);
        elseif case_num >=10
            dir = strcat('Data\TrainingMatData\Case',num2str(case_num),'.mat');
            exam = load(dir);
            dir_seg = strcat('Data\TrainingMatData\Case',num2str(case_num),'_seg.mat');
            exam_seg = load(dir_seg);
        end
    elseif type == 'nor0'
        if case_num < 0 | case_num>49
            disp('Incorrect case number!');    
        elseif case_num < 10
            dir = strcat('Data\TrainingNormData\Case0',num2str(case_num),'.mat');
            exam = load(dir);
            dir_seg = strcat('Data\TrainingNormData\Case0',num2str(case_num),'_seg.mat');
            exam_seg = load(dir_seg);
        elseif case_num >=10
            dir = strcat('Data\TrainingNormData\Case',num2str(case_num),'.mat');
            exam = load(dir);
            dir_seg = strcat('Data\TrainingNormData\Case',num2str(case_num),'_seg.mat');
            exam_seg = load(dir_seg);
        end
    elseif type == 'nor1'
        if case_num < 0 | case_num>49
            disp('Incorrect case number!');    
        elseif case_num < 10
            dir = strcat('Data\TrainingNormN4ITKData\Case0',num2str(case_num),'.mat');
            exam = load(dir);
            dir_seg = strcat('Data\TrainingNormN4ITKData\Case0',num2str(case_num),'_seg.mat');
            exam_seg = load(dir_seg);
        elseif case_num >=10
            dir = strcat('Data\TrainingNormN4ITKData\Case',num2str(case_num),'.mat');
            exam = load(dir);
            dir_seg = strcat('Data\TrainingNormN4ITKData\Case',num2str(case_num),'_seg.mat');
            exam_seg = load(dir_seg);
        end
    else
        disp('Incorrect type: choose raws, nor0 or nor1');
    end
end

