clear all
clc

h = waitbar(0,'Data normalization...');
%dodatkowa normalizacja jasności: 0 - bez wstępnej, 1 - N4ITK
type = 1;
for case_num = 0:49

    [exam,exam_seg] = load_case(case_num,'raws');

    %"podmiana" danych obrazowych na dane znormalizowane (intensywnościowo)
    exam.Case.data  = intensity_normalization( case_num, type );
    
    %"podmiana" danych obrazowych na dane znormalizowane rozmiarowo
    [exam, exam_seg] = size_normalization( exam,exam_seg, type );

    save_normcase(case_num, exam, exam_seg, type);
    waitbar(case_num/49,h,'Data normalization...');

end

disp('Data normalization finished');
waitbar(1,h,'Data normalization finished!');
close(h)

