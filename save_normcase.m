function save_normcase( case_num,exam,exam_seg,type )
%Function save_case allows to save normalized data to .mat files
%args: case_num - int
%      exam - case structure
%      exam_seg - segmentation case structure
%      type - 0: raws, 1: N4ITK
Case = exam.Case;
Case_seg = exam_seg.Case_seg;

switch type
    case 0
         if case_num < 0 | case_num>49
            disp('Incorrect train case number!');    
        elseif case_num < 10
            dir = strcat('Data\TrainingNormData\Case0',num2str(case_num),'.mat');
            save(dir,'Case');
            dir_seg = strcat('Data\TrainingNormData\Case0',num2str(case_num),'_seg.mat');
            save(dir_seg,'Case_seg');
        elseif case_num >=10
            dir = strcat('Data\TrainingNormData\Case',num2str(case_num),'.mat');
            save(dir,'Case');
            dir_seg = strcat('Data\TrainingNormData\Case',num2str(case_num),'_seg.mat');
            save(dir_seg,'Case_seg');
         end        
    case 1
        if case_num < 0 | case_num>49
            disp('Incorrect train case number!');    
        elseif case_num < 10
            dir = strcat('Data\TrainingNormN4ITKData\Case0',num2str(case_num),'.mat');
            save(dir,'Case');
            dir_seg = strcat('Data\TrainingNormN4ITKData\Case0',num2str(case_num),'_seg.mat');
            save(dir_seg,'Case_seg');
        elseif case_num >=10
            dir = strcat('Data\TrainingNormN4ITKData\Case',num2str(case_num),'.mat');
            save(dir,'Case');
            dir_seg = strcat('Data\TrainingNormN4ITKData\Case',num2str(case_num),'_seg.mat');
            save(dir_seg,'Case_seg');
         end                
end

end

