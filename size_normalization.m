function [ exam,exam_seg ] = size_normalization( exam,exam_seg,type )
% resizing
% sprowadzenie obraz�w do wsp�lnej przestrzeni rozmiar�w fizycznych
% 0.625 x 0.625 x 3.6
def_res = 0.625;
def_res_z = 3.6;

scale = exam.Case.spacing(1)/def_res;
scale_z = exam.Case.spacing(3)/def_res_z;

% exam.Case.data = imresize(exam.Case.data,scale);
% exam_seg.Case_seg.data = imresize(exam_seg.Case_seg.data,scale);

% desired output dimensions
nx = floor(size(exam.Case.data,1)*scale);
ny = floor(size(exam.Case.data,2)*scale);
nz = floor(size(exam.Case.data,3)*scale_z);

switch type
    case 0
        %case nor0
        [x y z]=...
           ndgrid(linspace(1,size(exam.Case.data,1),ny),...
                  linspace(1,size(exam.Case.data,2),nx),...
                  linspace(1,size(exam.Case.data,3),nz));
        exam.Case.data = interp3(exam.Case.data,x,y,z);
    case 1
        %case nor1
        [y x z]=...
           ndgrid(linspace(1,size(exam.Case.data,1),ny),...
                  linspace(1,size(exam.Case.data,2),nx),...
                  linspace(1,size(exam.Case.data,3),nz));
        exam.Case.data = interp3(exam.Case.data,x,y,z);
end
        
%case_seg
[x1 y1 z1]=...
   ndgrid(linspace(1,size(exam_seg.Case_seg.data,1),ny),...
          linspace(1,size(exam_seg.Case_seg.data,2),ny),...
          linspace(1,size(exam_seg.Case_seg.data,3),nz));
exam_seg.Case_seg.data = interp3(exam_seg.Case_seg.data,x1,y1,z1);

%binaryzacja maski segmentacji po przeskalowaniu
exam_seg.Case_seg.data(exam_seg.Case_seg.data >= graythresh(exam_seg.Case_seg.data)) = 1;
exam_seg.Case_seg.data(exam_seg.Case_seg.data < graythresh(exam_seg.Case_seg.data)) = 0;

end