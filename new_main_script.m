clear all
clc

%dividing trainig images: with endorectal coil and raw
erc_data = [0,2:12,38:49];
raw_data = [1,13:37];

%load input train case
n=raw_data(10);

[exam,exam_seg] = load_case(raw_data(n), 'nor0');

% convert to grayscale image (values 0.0 - 1.0)
Image = mat2gray(exam.Case.data);
Image_seg = mat2gray(exam_seg.Case_seg.data);

%visualization of horizontal slices
Image = permute(Image, [1 2 4 3]);
Image_seg = permute(Image_seg, [1 2 4 3]);

Image_fuzed = Image;

for k=1:size(Image_seg,4)
    [~, threshold] = edge(Image_seg(:,:,1,k), 'sobel');
    fudgeFactor = .5;
    BWs = edge(Image_seg(:,:,1,k),'sobel', threshold * fudgeFactor);
    
    BWoutline = bwperim(BWs);
    Segout = Image_fuzed(:,:,1,k); 
    Segout(BWoutline) = 255; 
    
    Image_fuzed(:,:,1,k)=Segout;
end

figure
implay(Image_fuzed);