%gradients trials

clear all;
close all;
clc

%% LOADING DATA
%dividing trainig images: with endorectal coil (erc) and raw
raw_data = [1,13:37];
case_num=raw_data(1);
[exam,exam_seg] = load_case(case_num, 'nor0');

% convert to grayscale image (values 0.0 - 1.0)
Img = exam.Case.data(:,:,floor(size(exam.Case.data,3)/2));
Img_seg = exam_seg.Case_seg.data(:,:,floor(size(exam_seg.Case_seg.data,3)/2));
Img_seg_edge = edge(Img_seg);
Img_contour = imfuse(Img,Img_seg_edge, 'blend');

s = [1; 1];
dx = [1, -1];
dy = [1; -1];

gx = conv2(conv2(Img,dx,'same'),s,'same');
gy = conv2(conv2(Img,dy,'same'),s','same');
