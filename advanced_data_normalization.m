%Normalizacja danych z wykorzystaniem metod stosowanych dla m�zgu
%linki i opisy w wordzie "wa�ne strony!"

clear all
clc

addpath('MRI_intensity_normalization-master')
addpath('MRI_intensity_normalization-master\nifti_tools')

% training_image_path = cell(2,1);
% training_image_path{1} = "C:\Users\ANIA\Documents\MATLAB\Prostate_segment\Data\TrainingData\Case00_corr";
% training_image_path{2} = "C:\Users\ANIA\Documents\MATLAB\Prostate_segment\Data\TrainingData\Case38_corr";

dirs = ['C:\Users\ANIA\Documents\MATLAB\Prostate_segment\Data\TrainingData\Case00_corr';
    'C:\Users\ANIA\Documents\MATLAB\Prostate_segment\Data\TrainingData\Case38_corr'];
celldata = cellstr(dirs);

learn_intensity_landmarks(celldata, 0, 1)