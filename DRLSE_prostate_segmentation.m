% prostate segmentation basing on DRLSE 

clear all;
close all;
clc

%% LOADING DATA
%dividing trainig images: with endorectal coil (erc) and raw
erc_data = [0,2:12,38:49];
raw_data = [1,13:37];

for k=1:size(raw_data,2)

case_num=raw_data(k);
[exam,exam_seg] = load_case(case_num, 'nor0');

% convert to grayscale image (values 0.0 - 1.0)
Img = exam.Case.data(:,:,floor(size(exam.Case.data,3)/2));
Img_seg = exam_seg.Case_seg.data(:,:,floor(size(exam_seg.Case_seg.data,3)/2));
Img_seg_edge = edge(Img_seg);
Img_contour = imfuse(Img,Img_seg_edge, 'blend');

Img = Img.*255;

%% DRLSE
% parameter setting
timestep=5;  % time step
mu=0.2/timestep;  % coefficient of the distance regularization term R(phi)
iter_inner=10;
iter_outer=100;
lambda=4; % coefficient of the weighted length term L(phi)
alfa=-1.5;  % coefficient of the weighted area term A(phi)
epsilon=1.5; % papramater that specifies the width of the DiracDelta function

sigma=2;     % scale parameter in Gaussian kernel
G=fspecial('gaussian',15,sigma);
Img_smooth=conv2(Img,G,'same');  % smooth image by Gaussian convolution
Img_edge=edge(Img_smooth,'Canny');
Img_add = imadd(Img_smooth,Img_edge*100);
[Ix,Iy]=gradient(Img_add);
f=Ix.^2+Iy.^2;
g=1./(1+f);  % edge indicator function.

% initialize LSF as binary step function
c0=2;
initialLSF=c0*ones(size(Img));
% generate the initial region R0 as a rectangle specified by user
figure(1), imshow(Img, [0 255]);
title('Draw an ellipse inside a prostate. Double-click on the ROI when finished.');
set(gcf,'units','normalized','outerposition',[0 0 1 1])

% rect = rectangle('Position',[size(Img,1)/2-10, size(Img,2)/2-8, 20, 16]);
initialLSF(size(Img,1)/2-4:size(Img,1)/2+4, size(Img,1)/2-8:size(Img,1)/2+8) = -c0;
% initialLSF(rect(2):rect(2)+rect(4), rect(1):rect(1)+rect(3))=-c0;  

% ellipse_start = imellipse;
% position = wait(ellipse_start);
% BW = createMask(ellipse_start);
% initialLSF(BW==1) = -c0;

phi=initialLSF;

figure(2); subplot(121)
mesh(-phi);   % for a better view, the LSF is displayed upside down
hold on;  contour(phi, [0,0], 'r','LineWidth',2);
title('Initial level set function');
view([-80 35]);

figure(3);
imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
title('Initial zero level contour');
pause(0.5);

potential=2;  
if potential ==1
    potentialFunction = 'single-well';  % use single well potential p1(s)=0.5*(s-1)^2, which is good for region-based model 
elseif potential == 2
    potentialFunction = 'double-well';  % use double-well potential in Eq. (16), which is good for both edge and region based models
else
    potentialFunction = 'double-well';  % default choice of potential function
end


% start level set evolution
for n=1:iter_outer
    phi = drlse_edge(phi, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);
    if mod(n,2)==0
        figure(3);
        imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
        drawnow
    end
end

% refine the zero level contour by further level set evolution with alfa=0
alfa=0;
iter_refine = 20;
phi = drlse_edge(phi, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);
figure(3);
imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');

finalLSF=phi;
figure(4);
subplot(121), imagesc(Img); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
hold on;  contour(phi, [0,0], 'r');
str=['Final zero level contour, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
title(str);
subplot(122), imshow(Img_contour);
title('Reference segmentation');
set(gcf,'units','normalized','outerposition',[0 0 1 1]);
tit = strcat('Results/DRLSE_v1/Case',num2str(case_num),'_DRLSE.jpg');
saveas(gcf,tit);

pause(2);
figure(2); subplot(122)
mesh(-finalLSF); % for a better view, the LSF is displayed upside down
hold on;  contour(phi, [0,0], 'r','LineWidth',2);
str=['Final level set function, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
title(str);
axis on;

end

close all