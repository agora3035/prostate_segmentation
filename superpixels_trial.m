clear all
close all
clc

raw_data = [1,13:37];

for k=1:size(raw_data,2)
    [exam,exam_seg] = load_case(k, 'nor0');

    p3d = squeeze(exam.Case.data);

    Img = p3d(:,:,floor(size(exam.Case.data,3)/2));
    Img_edge = edge(Img,'Canny');
    Img_seg = exam_seg.Case_seg.data(:,:,floor(size(exam_seg.Case_seg.data,3)/2));
    Img_seg_edge = edge(Img_seg);
    Img_contour = imfuse(Img_edge,Img_seg_edge, 'blend');
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    imshow(Img_edge);
    str=['Case ', num2str(raw_data(k))];
    title(str);
    keyboard
end
%%
N = 50;
[L,NumLabels] = superpixels3(p3d,N,'Compactness',0.01,'NumIterations',50);

imSize = size(p3d);
imPlusBoundaries = zeros(imSize(1),imSize(2),3,imSize(3),'uint8');
for plane = 1:imSize(3)
  BW = boundarymask(L(:, :, plane));
  % Create an RGB representation of this plane with boundary shown
  % in cyan.
  imPlusBoundaries(:, :, :, plane) = imoverlay(p3d(:, :, plane), BW, 'cyan');
end

implay(imPlusBoundaries,5)

% pixelIdxList = label2idx(L);
% meanA = zeros(size(p3d),'like',p3d);
% for superpixel = 1:NumLabels
%      memberPixelIdx = pixelIdxList{superpixel};
%      meanA(memberPixelIdx) = mean(p3d(memberPixelIdx));
% end
% implay([p3d meanA],5);

