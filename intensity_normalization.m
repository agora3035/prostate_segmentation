function [ normalized_data ] = intensity_normalization( case_num,type )
 %normalizacja Z-Score intensywno�ci obraz�w
 % type:  0 - surowe pliki
 %        1 - pliki .nrrd po N4ITK bias correction

 switch type
     case 0  %raws
         exam = load_case(case_num,'raws');
         im_data = exam.Case.data;
                  
     case 1  % .nrrd files - files after N4ITK bias correction
        addpath('nrrdread');

        if case_num <0 | case_num>49
            fprinf("Wrong case number!");
        elseif case_num<10
            dir = strcat('Data\TrainingData_biascorr\Case0',num2str(case_num),'.nrrd');
        else
            dir = strcat('Data\TrainingData_biascorr\Case',num2str(case_num),'.nrrd');
        end
        im_data = nrrdread(dir);
 end
 
 %z-score i sprowadzenie do przestrzeni warto�ci (0 - 1)
 normalized_data = mat2gray(zscore(im_data));

 % %usuni�cie szumu - warto�ci bliskich zeru (5% -> szum)
 % minval = min(min(min(I)));
 % maxval = max(max(max(I)));
 % I(I<(0.05*maxval)) = minval;
 % I(I>(0.95*maxval)) = maxval;

end

