clear all
clc
%%
%extract metadata
h = waitbar(0,'Metadata extracting...');
data_info_header = ["case_num","mat_size","origin","spacing","orientation","index","D","paddingValue","Max_chunk_size","ndim"];
data_info_tab = strings(51,10);
data_seg_info_tab = strings(51,10);
data_info_tab(1,:) = data_info_header;
data_seg_info_tab(1,:) = data_info_header;

for i = 0:49
    [exam,exam_seg] = load_case( i , 'train');
    
    data_info_tab(i+2,1) = i;
    data_info_tab(i+2,2) = mat2str(size(exam.Case.data));
    data_info_tab(i+2,3) = mat2str(exam.Case.origin);
    data_info_tab(i+2,4) = mat2str(exam.Case.spacing);
    data_info_tab(i+2,5) = mat2str(exam.Case.orientation);
    data_info_tab(i+2,6) = mat2str(exam.Case.index);
    data_info_tab(i+2,7) = mat2str(exam.Case.D);
    data_info_tab(i+2,8) = exam.Case.paddingValue;
    data_info_tab(i+2,9) = exam.Case.MAX_CHUNK_SIZE;
    data_info_tab(i+2,10) = exam.Case.ndimensions;
    
    data_seg_info_tab(i+2,1) = i;
    data_seg_info_tab(i+2,2) = mat2str(size(exam_seg.Case_seg.data));
    data_seg_info_tab(i+2,3) = mat2str(exam_seg.Case_seg.origin);
    data_seg_info_tab(i+2,4) = mat2str(exam_seg.Case_seg.spacing);
    data_seg_info_tab(i+2,5) = mat2str(exam_seg.Case_seg.orientation);
    data_seg_info_tab(i+2,6) = mat2str(exam_seg.Case_seg.index);
    data_seg_info_tab(i+2,7) = mat2str(exam_seg.Case_seg.D);
    data_seg_info_tab(i+2,8) = exam_seg.Case_seg.paddingValue;
    data_seg_info_tab(i+2,9) = exam_seg.Case_seg.MAX_CHUNK_SIZE;
    data_seg_info_tab(i+2,10) = exam_seg.Case_seg.ndimensions;
    
    waitbar(i/49,h,'Metadata extracting...');
end

xlswrite('Data\data_info.xls',data_info_tab);
xlswrite('Data\data_seg_info.xls',data_seg_info_tab);

close(h);