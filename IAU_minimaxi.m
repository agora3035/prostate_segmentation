function [mini,maxi] = IAU_minimaxi(Img,Img_seg)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

idx = find(Img_seg);
pvals = Img(idx);
mini = min(pvals);
maxi = max(pvals);
range = maxi - mini;
mini = mini + 0.1*range;
maxi = maxi - 0.2*range;
end

