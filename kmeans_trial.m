clear all
clc

numClusters = 2;
tic
for n = 0 : 49

    [exam,exam_seg] = load_case( n , 'nor0');

    I_init = exam.Case.data(:,:,round(size(exam.Case.data,3)/2));
    I_seg = exam_seg.Case_seg.data(:,:,round(size(exam_seg.Case_seg.data,3)/2));

    I = double(I_init);

    fig = figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(131), imshow(I_init);
    subplot(133), imshow(I_seg);

%     nrows = size(I,1);
%     ncols = size(I,2);
%     I = reshape(I,nrows*ncols,1);
% 
%     [idx,c] = kmeans(I,numClusters,'Replicates',3);
%     p = reshape(idx,nrows,ncols);
%     subplot(132), imshow(p,[]);

    [lb,center] = adaptcluster_kmeans(I_init);
    subplot(132), imshow(lb,[]);
    
%     print(fig,'MySavedPlot','-jpg')
end
toc
disp('Done!')

% ab=imread('cameraman.tif');
% ab = double(ab);
% 
% nrows = size(ab,1);
% ncols = size(ab,2);
% ab = reshape(ab,nrows*ncols,1);
% 
% nColors = 4;
% % repeat the clustering 3 times to avoid local minima
% [cluster_idx, cluster_center] = kmeans(ab,nColors,'distance','sqEuclidean', 'Replicates', 3);
% 
% pixel_labels = reshape(cluster_idx,nrows,ncols);
% imshow(pixel_labels,[]);