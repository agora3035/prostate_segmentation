clear all
clc
%%
[exam,exam_seg] = load_case( 0 , 'nor0');
% Image = mat2gray(exam.Case.data);

I = exam.Case.data(:,:,15);
I_seg = exam_seg.Case_seg.data(:,:,15);

[rows,cols] = find(I); coors(:,1) = rows; coors(:,2) = cols(:,1);
[features,validPoints] = extractFeatures(I,coors);

for i = 1 : length(validPoints)
    mask_val(i) = I_seg(validPoints(i,1),validPoints(i,2));
end
mask_val = mask_val';

%%
[rows,cols] = find(I_seg);

p_coors(:,1) = rows;
p_coors(:,2) = cols(:,1);

[p_features,p_validPoints] = extractFeatures(I,p_coors);

[rows,cols] = find(~I_seg);

np_coors(:,1) = rows;
np_coors(:,2) = cols(:,1);

[np_features,np_validPoints] = extractFeatures(I,np_coors);


[features,validPoints] = extractFeatures(I,p_coors);



