close all
clear all
clc

all_data = 0:49;
[Img_univ_mask,coors] = IAU_mask_creation(all_data);

%%
raw_data = [1,13:37];
for k=1:size(raw_data,2)
    [exam,exam_seg] = load_case(raw_data(k), 'nor0');
    p3d = squeeze(exam.Case.data);
    p3d_mask = squeeze(exam_seg.Case_seg.data);
    Img_seg = p3d_mask(:,:,round(size(p3d_mask,3)/2));
    
    Img3d = medfilt3(p3d,[5,5,5]);
    Img = Img3d(:,:,round(size(Img3d,3)/2));
    se = strel('disk',3);
    tophatFiltered = imtophat(Img,se);
    bothatFiltered = imbothat(Img,se);
    smoothImg = Img + tophatFiltered - bothatFiltered;

    c0 = size(Img,1)/2;
    Img_crop = imcrop(smoothImg,[c0-100 c0-100 199 199]);

    p0=coors(3);
    for p=coors(3):coors(4)
        row_avg(p-p0+1,k) = mean(Img_crop(coors(1):coors(2),p));
    end
    
    [mini,maxi] = IAU_minimaxi(Img,Img_seg);
    
    BW_mini = imbinarize(Img, mini);
    BW_maxi = imbinarize(Img, maxi);
    BW = BW_mini - BW_maxi;
    BW = logical(BW);
 
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(221), imshow(Img)
    subplot(222), imshow(Img_seg)
    subplot(223), imshow(BW)
        
    se1 = strel('disk',5);
    erodedBW = imerode(gpuArray(BW),se1);
    se2 = strel('disk',3);
    closedBW = imclose(erodedBW,se2);
    subplot(224), imshow(closedBW)
end