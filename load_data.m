clear all
clc
%% loading original input data and saving it in Matlab friendly format
disp('Input data loading...');
h = waitbar(0,'Loading...');
%%
%Part1
Case = read_mhd('Data\TrainingData\TrainingData_Part1\Case00.mhd');
Case_seg = read_mhd('Data\TrainingData\TrainingData_Part1\Case00_segmentation.mhd');
save('Data\TrainingMatData\Case00.mat','Case');
save('Data\TrainingMatData\Case00_seg.mat','Case_seg');
for i= 1:9
    disp(i);
    dir = strcat('Data\TrainingData\TrainingData_Part1\Case0',num2str(i),'.mhd');
    Case = read_mhd(dir);
    dir_seg = strcat('Data\TrainingData\TrainingData_Part1\Case0',num2str(i),'_segmentation.mhd');
    Case_seg = read_mhd(dir_seg);
    sdir = strcat('Data\TrainingMatData\Case0',num2str(i),'.mat');
    save(sdir,'Case');
    sdir_seg = strcat('Data\TrainingMatData\Case0',num2str(i),'_seg.mat');
    save(sdir_seg,'Case_seg');
    waitbar(i/50,h,'Loading...');
end
for i= 10:25
    disp(i);
    dir = strcat('Data\TrainingData\TrainingData_Part1\Case',num2str(i),'.mhd');
    Case = read_mhd(dir);
    dir_seg = strcat('Data\TrainingData\TrainingData_Part1\Case',num2str(i),'_segmentation.mhd');
    Case_seg = read_mhd(dir_seg);
    sdir = strcat('Data\TrainingMatData\Case',num2str(i),'.mat');
    save(sdir,'Case');
    sdir_seg = strcat('Data\TrainingMatData\Case',num2str(i),'_seg.mat');
    save(sdir_seg,'Case_seg');
    waitbar(i/50,h,'Loading...');
end

%%
%Part2
for i= 26:37
    disp(i);
    dir = strcat('Data\TrainingData\TrainingData_Part2\Case',num2str(i),'.mhd');
    Case = read_mhd(dir);
    dir_seg = strcat('Data\TrainingData\TrainingData_Part2\Case',num2str(i),'_segmentation.mhd');
    Case_seg = read_mhd(dir_seg);
    sdir = strcat('Data\TrainingMatData\Case',num2str(i),'.mat');
    save(sdir,'Case');
    sdir_seg = strcat('Data\TrainingMatData\Case',num2str(i),'_seg.mat');
    save(sdir_seg,'Case_seg');
    waitbar(i/50,h,'Loading...');
end

%%
%Part3
for i= 38:49
    disp(i);
    dir = strcat('Data\TrainingData\TrainingData_Part3\Case',num2str(i),'.mhd');
    Case = read_mhd(dir);
    dir_seg = strcat('Data\TrainingData\TrainingData_Part3\Case',num2str(i),'_segmentation.mhd');
    Case_seg = read_mhd(dir_seg);
    sdir = strcat('Data\TrainingMatData\Case',num2str(i),'.mat');
    save(sdir,'Case');
    sdir_seg = strcat('Data\TrainingMatData\Case',num2str(i),'_seg.mat');
    save(sdir_seg,'Case_seg');
    waitbar(i/50,h,'Loading...');
end
disp('Loading finished.');
waitbar(1,h,'Loading...');
close(h)

