%% FIRST TRIAL FILE
clear all
close all
clc;
fprintf('\nFEATURE SELECTION TOOLBOX v 5.0 2017 - For Matlab \n');
% Include dependencies
addpath('C:\Users\ANIA\Documents\MATLAB\Add-Ons\Toolboxes\Feature Selection Library\code\FSLib_v5.2_2017\lib'); % dependencies
addpath('C:\Users\ANIA\Documents\MATLAB\Add-Ons\Toolboxes\Feature Selection Library\code\FSLib_v5.2_2017\methods'); % FS methods
addpath(genpath('C:\Users\ANIA\Documents\MATLAB\Add-Ons\Toolboxes\Feature Selection Library\code\FSLib_v5.2_2017\lib\drtoolbox'));

% Select a feature selection method from the list
listFS = {'ILFS','InfFS','ECFS','mrmr','relieff','mutinffs','fsv','laplacian','mcfs','rfe','L0','fisher','UDFS','llcfs','cfs'};

[ methodID ] =  5;
selection_method = listFS{methodID}; % Selected

%load data
[exam,exam_seg] = load_case( 0 , 'nor0');
% Image = mat2gray(exam.Case.data);

I = exam.Case.data(:,:,15);
I_seg = exam_seg.Case_seg.data(:,:,15);

% [rows,cols] = find(I); 
% coors(:,1) = rows; 
% coors(:,2) = cols(:,1);
% %extract features values
% [features,validPoints] = extractFeatures(I,coors);

corners = detectHarrisFeatures(I);
[features, valid_corners] = extractFeatures(I, corners);

X = double(features.Features);

for i = 1 : length(validPoints)
    mask_val(i) = I_seg(validPoints(i,1),validPoints(i,2));
end
clear validPoints;
%load mask values - prostate or not (1 or 0)
Y = mask_val'; clear mask_val;
Y (Y == 0) = -1;

% Randomly partitions observations into a training set and a test
% set using stratified holdout
P = cvpartition(Y,'Holdout',0.20);

X_train = double( X(P.training,:) );
Y_train = Y(P.training); % labels: neg_class -1, pos_class +1

X_test = double( X(P.test,:) );
Y_test = Y(P.test); % labels: neg_class -1, pos_class +1

% number of features
numF = size(X_train,2);


% feature Selection on training data
switch lower(selection_method)
    case 'ilfs'
        % Infinite Latent Feature Selection - ICCV 2017
        [ranking, weights, subset] = ILFS_auto(X_train, Y_train , 4, 0 )
    case 'mrmr'
        ranking = mRMR(X_train, Y_train, numF);
        
    case 'relieff'
        [ranking, w] = reliefF( X_train, Y_train, 20);
        
    case 'mutinffs'
        [ ranking , w] = mutInfFS( X_train, Y_train, numF );
        
    case 'fsv'
        [ ranking , w] = fsvFS( X_train, Y_train, numF );
        
    case 'laplacian'
        W = dist(X_train');
        W = -W./max(max(W)); % it's a similarity
        [lscores] = LaplacianScore(X_train, W);
        [junk, ranking] = sort(-lscores);
        
    case 'mcfs'
        % MCFS: Unsupervised Feature Selection for Multi-Cluster Data
        options = [];
        options.k = 5; %For unsupervised feature selection, you should tune
        %this parameter k, the default k is 5.
        options.nUseEigenfunction = 4;  %You should tune this parameter.
        [FeaIndex,~] = MCFS_p(X_train,numF,options);
        ranking = FeaIndex{1};
        
    case 'rfe'
        ranking = spider_wrapper(X_train,Y_train,numF,lower(selection_method));
        
    case 'l0'
        ranking = spider_wrapper(X_train,Y_train,numF,lower(selection_method));
        
    case 'fisher'
        ranking = spider_wrapper(X_train,Y_train,numF,lower(selection_method));
        
    case 'inffs'
        % Infinite Feature Selection 2015 updated 2016
        alpha = 0.5;    % default, it should be cross-validated.
        sup = 1;        % Supervised or Not
        [ranking, w] = infFS( X_train , Y_train, alpha , sup , 0 );    
        
    case 'ecfs'
        % Features Selection via Eigenvector Centrality 2016
        alpha = 0.5; % default, it should be cross-validated.
        ranking = ECFS( X_train, Y_train, alpha )  ;
        
    case 'udfs'
        % Regularized Discriminative Feature Selection for Unsupervised Learning
        nClass = 2;
        ranking = UDFS(X_train , nClass ); 
        
    case 'cfs'
        % BASELINE - Sort features according to pairwise correlations
        ranking = cfs(X_train);     
        
    case 'llcfs'   
        % Feature Selection and Kernel Learning for Local Learning-Based Clustering
        ranking = llcfs( X_train );
        
    otherwise
        disp('Unknown method.')
end
%%
k = 2; % select the first 2 features

% Use a linear support vector machine classifier
svmStruct = svmtrain(X_train(:,ranking(1:k)),Y_train,'showplot',true);
C = svmclassify(svmStruct,X_test(:,ranking(1:k)),'showplot',true);
err_rate = sum(Y_test~= C)/P.TestSize; % mis-classification rate
conMat = confusionmat(Y_test,C); % the confusion matrix

disp('X_train size')
size(X_train)

disp('Y_train size')
size(Y_train)

disp('X_test size')
size(X_test)

disp('Y_test size')
size(Y_test)


fprintf('\nMethod %s (Linear-SVMs): Accuracy: %.2f%%, Error-Rate: %.2f \n',selection_method,100*(1-err_rate),err_rate);
