function [Img_univ_mask,coors] = IAU_mask_creation(datavec);

Img_seg_sum=zeros(200,200);

    for k=1:size(datavec,2)
        [exam,exam_seg] = load_case(datavec(k), 'nor0');
        p3d_seg = squeeze(exam_seg.Case_seg.data);
        Img_seg = p3d_seg(:,:,floor(size(exam_seg.Case_seg.data,3)/2));

        c0 = size(Img_seg,1)/2;
        Img_seg_crop = imcrop(Img_seg,[c0-100 c0-100 199 199]);
        Img_seg_sum = Img_seg_sum + Img_seg_crop;
    end

    Img_seg_mean = Img_seg_sum/k;
    Img_seg_mean_BW = imbinarize(Img_seg_mean);
    Img_seg_mean_edges = edge(Img_seg_mean_BW);
    [x, y] = find(Img_seg_mean_edges);
    xmin = min(x);
    xmax = max(x);
    ymin = min(y);
    ymax = max(y);
    Img_univ_mask = zeros(size(Img_seg_mean_edges));
    Img_univ_mask(xmin:xmax,ymin:ymax) = 1;
    coors = [xmin xmax ymin ymax];
end