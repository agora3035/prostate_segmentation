clear all
close all
clc

raw_data = [1,13:37];

Img_sum = zeros(200,200);
Img_seg_sum = zeros(200,200);

for k=1:size(raw_data,2)
    [exam,exam_seg] = load_case(k, 'nor0');

    p3d = squeeze(exam.Case.data);
    p3d_seg = squeeze(exam_seg.Case_seg.data);

    Img = p3d(:,:,floor(size(exam.Case.data,3)/2));
    Img_seg = p3d_seg(:,:,floor(size(exam_seg.Case_seg.data,3)/2));
    
    c0 = size(Img,1)/2;    
    Img_crop = imcrop(Img,[c0-100 c0-100 199 199]);
    Img_seg_crop = imcrop(Img_seg,[c0-100 c0-100 199 199]);
    
    Img_sum = Img_sum + Img_crop;
    Img_seg_sum = Img_seg_sum + Img_seg_crop;
    
    Img_mean = Img_sum/k;
    Img_seg_mean = Img_seg_sum/k;
    subplot(221), imshow(Img_mean)
    subplot(222), imshow(Img_seg_mean)
end

%%
[exam,exam_seg] = load_case(1, 'nor0');
p3d = squeeze(exam.Case.data);
I = p3d;
J = histeq(I);
figure
subplot(121), imshow(I(:,:,12))
subplot(122), imhist(I(:,:,12))
figure
subplot(121), imshow(J(:,:,12))
subplot(122), imhist(J(:,:,12))

sigma = 1.2;
K = imgaussfilt3(J, sigma);
figure
subplot(121), imshow(K(:,:,12))
subplot(122), imhist(K(:,:,12))

BW = edge3(K,'approxcanny',0.6);
figure
subplot(121), montage(reshape(p3d,size(p3d)))
subplot(122), montage(reshape(BW,size(p3d)))